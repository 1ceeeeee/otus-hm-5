import React, { Component } from 'react';
import axios from 'axios';

type ClassComponentState = {
  url: string;
  response: string;
  error: string | null;
};

class ClassComponent extends Component<{}, ClassComponentState> {
  state: ClassComponentState = {
    url: '',
    response: '',
    error: null,
  };

  handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ url: event.target.value });
  };

  handleButtonClick = () => {
    axios
      .get(this.state.url)
      .then((response) => {
        this.setState({ response: JSON.stringify(response.data.fact), error: null });
      })
      .catch((error) => {
        this.setState({ response: '', error: error.message });
      });
  };

  render() {
    const { url, response, error } = this.state;

    return (
      <div>
        <input type="text" value={url} onChange={this.handleInputChange} />
        <button onClick={this.handleButtonClick}>Отправить</button>
        {error && <div style={{ color: 'red' }}>{error}</div>}
        {response && <div style={{ color: 'black' }}>{response}</div>}
      </div>
    );
  }
}

export default ClassComponent;