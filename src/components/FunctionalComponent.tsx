import React, { useState } from 'react';
import axios from 'axios';

export default function FunctionComponent() {
  const [url, setUrl] = useState('');
  const [response, setResponse] = useState('');
  const [error, setError] = useState<string | null>(null);

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setUrl(event.target.value);
  };

  const handleButtonClick = () => {
    axios
      .get(url)
      .then((response) => {
        setResponse(JSON.stringify(response.data.fact));
        setError(null);
      })
      .catch((error) => {
        setResponse('');
        setError(error.message);
      });
  };

  return (
    <div>
      <input type="text" value={url} onChange={handleInputChange} />
      <button onClick={handleButtonClick}>Отправить</button>
      {error && <div style={{ color: 'red' }}>{error}</div>}
      {response && <div style={{ color: 'black' }}>{response}</div>}
    </div>
  );
};
