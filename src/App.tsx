import './App.css';
import FunctionComponent from './components/FunctionalComponent';
import ClassComponent from './components/ClassComponent';


function App() {
  
  return (
    <div>
      <FunctionComponent />
      <ClassComponent/>
    </div>
  );
}

export default App;
